var gulp = require('gulp');
var concat = require('gulp-concat');
var webpack = require('webpack-stream');


gulp.task('default', function() {
  return gulp.src(['./public/js/*.js'])
      .pipe(concat('all.js'))
      .pipe(gulp.dest('./public'));
     
});

gulp.task('webpack', function() {
  return gulp.src('entry.js')
    .pipe(webpack())
    .pipe(gulp.dest('./dist'))
     
});

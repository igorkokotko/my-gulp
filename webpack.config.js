module.exports = {
    entry: "./entry.js",
    output: {
        path: __dirname,
        filename: "./dist/514d985adabb5e708d2e.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" }
        ]
    }
};
var http = require('http');
var express = require('express');
var app = express();

app.use(express.static('public'));
app.get('/', function(req, res) {
    res.sendFile(__dirname + 'index.html');
});

var server = http.createServer(app);


server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});